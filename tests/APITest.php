<?php

use ut_devops\ImmoScoutAPI\ImmoScoutAPI;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class TestScoutAPI extends ImmoScoutAPI
{
    public $token;
    public $secret;
    protected static $stack = null;

    public static $accessToken;
    public static $accessSecret;

    public static function setStack($stack)
    {
        TestScoutAPI::$stack = $stack;
    }

    public function saveRequestToken($token, $secret)
    {
    }
    public function restoreRequestToken()
    {
        return [
            "myToken",
            "mySecret",
        ];
    }

    public function saveAccessToken($token, $secret)
    {
    }
    public static function restoreAccessToken()
    {
        return [
            "myAaccessToken",
            "myAccessSecret",
        ];
    }

    // public static function createClient($key,
    //     $secret,
    //     $authorized = true,
    //     callable $commandToRequestTransformer = null,
    //     callable $responseToResultTransformer = null,
    //     HandlerStack $commandHandlerStack = null,
    //     array $config = []) {
    //     return new Client(['handler' => self::$stack]);
    // }

}

final class APITest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $mock = new MockHandler([
            new Response(200, ['X-Foo' => 'Bar'], 'Hello, World'),
            new Response(202, ['Content-Length' => 0]),
            new RequestException('Error Communicating with Server', new Request('GET', 'test')),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $container = [];
        $history = Middleware::history($container);
        $handlerStack->push($history);

        $this->client = TestScoutAPI::createClient("myKey", "mySecret");
        $this->client->client = new Client([
            'base_uri' => "http://localhost",
            'handler' => $handlerStack,
            'verify' => false,
        ]);
    }

    public function testDefaultDomain()
    {
        self::assertEquals($this->client->getDomain(), "sandbox-immobilienscout24.de");
    }

    public function testSwichingSandbox()
    {
        $this->client->useSandbox();
        self::assertEquals($this->client->getDomain(), "sandbox-immobilienscout24.de");
        $this->client->dontUseSandbox();
        self::assertEquals($this->client->getDomain(), "immobilienscout24.de");
    }

    // public function testSetUser()
    // {
    //     $this->client->setUser("test");
    //     self::assertEquals($this->client->user, "test");

    // }
    // public function testVerifyApplication()
    // {
    //     $this->client->verifyApplication("myUrl");
    // }

}
