<?php

namespace ut_devops\ImmoScoutAPI;

use ut_devops\ImmoScoutAPI\exceptions\AuthException;
use ut_devops\ImmoScoutAPI\exceptions\InvalidResponse;
use ut_devops\ImmoScoutAPI\exceptions\InvalidTokenException;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Psr\Http\Message\ResponseInterface;

/**
 * An example implementation for the immoscout24 rest api in php. This Client is not complete and only
 * implements some methods for the export/import api, but it should be quite easy to extend. Just look
 * at the later methods that show the interaction after the auth is done.
 */
abstract class ImmoScoutAPI extends GuzzleClient
{
    protected $consumerKey;
    protected $consumerKeySecret;
    protected $client = null;
    public $history = null;
    protected $user = 'me';
    protected $isSandbox = true;

    /**
     * should save the request token with tokenname + secret, the token can be temporarily saved within a session
     * this token is only needed during the "request an access token" phase.
     *
     * @param string $token
     * @param string $secret
     *
     * @return void
     */
    abstract public function saveRequestToken($token, $secret);

    /**
     * restores the temporarily saved request token.
     *
     * @return array with 2 elements: first element is the token name, second element is the secret
     */
    abstract public function restoreRequestToken();

    /**
     * saves the access token, the information should be stored persistently, for example in a database or file.
     * the progress of getting an access token only needs to be done once per user.
     *
     * @param string $token
     * @param string $secret
     *
     * @return void
     */
    abstract public function saveAccessToken($token, $secret);

    /**
     * restores the saved access token.
     *
     * @return array with 2 elements: first element is the token name, second element is the secret
     */
    abstract public static function restoreAccessToken();

    /**
     * checks for the right format of a token.
     *
     * @param array a token array [name, secret]
     *
     * @return void
     */
    protected function validateToken($tokenArray)
    {
        if (!is_array($tokenArray)) {
            throw new InvalidTokenException('restored Token is not an Array need to be of the form [token, token_secret]');
        }
        if (!isset($tokenArray[1])) {
            throw new InvalidTokenException('restored Token array does not have a second element needs to be of the form [token, token_secret]');
        }
    }

    protected function getValidatedRequestToken()
    {
        $token = $this->restoreRequestToken();
        $this->validateToken($token);

        return $token;
    }

    protected function getValidatedAccessToken()
    {
        $token = static::restoreAccessToken();
        $this->validateToken($token);

        return $token;
    }

    /**
     * use the sandbox api.
     *
     * @return boolean
     */
    public function useSandbox()
    {
        $this->isSandbox = true;
    }

    /**
     * use the production api.
     *
     * @return boolean
     */
    public function dontUseSandbox()
    {
        $this->isSandbox = false;
    }

    /**
     * creates a new client.
     *
     * @param string $key        consumer key
     * @param string $secret     consumer secret
     * @param bool   $authorized this should only be false for non 3-legged-authented requests
     *
     * @return static
     */
    public static function createClient(
        $key,
        $secret,
        $authorized = true,
        callable $commandToRequestTransformer = null,
        callable $responseToResultTransformer = null,
        HandlerStack $commandHandlerStack = null,
        array $config = []) {

        $token = '';
        $token_secret = '';
        if ($authorized) {
            $tokenArray = static::restoreAccessToken();
            if ($tokenArray && is_array($tokenArray)) {
                $token = $tokenArray[0];
                $token_secret = $tokenArray[1];
            }
        }

        $stack = HandlerStack::create();
        $oAuth = new Oauth1([
            'consumer_key' => $key,
            'consumer_secret' => $secret,
            'token' => $token,
            'token_secret' => $token_secret,
        ]);
        $stack->push($oAuth);
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'handler' => $stack,
            'auth' => 'oauth',
        ]);

        $newGuzzleClient = new static(
            $client, new Description([]),
            $commandToRequestTransformer,
            $responseToResultTransformer,
            $commandHandlerStack,
            $config
        );
        $newGuzzleClient->client = $client;
        $newGuzzleClient->oAuth = $oAuth;
        $newGuzzleClient->consumerKey = $key;
        $newGuzzleClient->consumerKeySecret = $secret;

        return $newGuzzleClient;
    }

    /**
     * get is24 domain depending on the sandbox.
     *
     * @return string base url
     */
    public function getDomain()
    {
        return $this->isSandbox ? 'sandbox-immobilienscout24.de' : 'immobilienscout24.de';
    }

    /**
     * get base api url depending on the sandbox.
     *
     * @return string base url
     */
    protected function getBaseUrl()
    {
        $domain = $this->getDomain();

        return 'https://rest.' . $domain . '/restapi/';
    }

    /**
     * get the current uri, this can be used to return to the current site after authentification.
     *
     * @return string current url
     */
    protected static function getSelfURI()
    {
        if (!isset($_SERVER) || !isset($_SERVER['HTTP_HOST']) || !isset($_SERVER['REQUEST_URI'])) {
            throw new \Exception("coudn't detect request uri for callback (please specify one)");
        }
        $url = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';
        $url .= '://';
        $url .= $_SERVER['HTTP_HOST'];
        $url .= $_SERVER['REQUEST_URI'];

        return $url;
    }

    /**
     * sets the user for the api, this is only needed if you want to have multiple users,
     * default authenticated user is "me".
     *
     * @param string $user
     *
     * @return void
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * this creates a default oAuth with a token and secret, this method is used for calls
     * before the app is authenticated, so for requesting a request token and requesting an access token.
     *
     * @param string $uri
     * @param array  $oAuthData
     *
     * @return ResponseInterface
     */
    protected function callOAuth($uri, $oAuthData)
    {
        $oAuthData['consumer_key'] = $this->consumerKey;
        $oAuthData['consumer_secret'] = $this->consumerKeySecret;
        $oAuthData['token'] = isset($oAuthData['token']) ? $oAuthData['token'] : '';
        $oAuthData['token_secret'] = isset($oAuthData['token_secret']) ? $oAuthData['token_secret'] : '';

        $stack = HandlerStack::create();
        $oAuth = new Oauth1($oAuthData);
        $stack->push($oAuth);

        $client = new Client([
            'base_uri' => $this->getBaseUrl(),
            'handler' => $stack,
            'auth' => 'oauth',
        ]);

        return $client->get($uri);
    }

    /**
     * get a request token.
     *
     * @param string $callback the callack url, where you get redirected after you authenticated the application on is24
     *
     * @return void
     */
    protected function requestRequestToken($callback)
    {
        try {
            $res = static::callOAuth('/restapi/security/oauth/request_token', [
                'callback' => $callback,
            ]);
        } catch (ClientException $ex) {
            throw new AuthException('Failed to get the request token');
        }
        parse_str((string) $res->getBody(), $body);
        if (!isset($body['oauth_token']) || !isset($body['oauth_token_secret'])) {
            throw new InvalidResponse('Could not parse the requested request token');
        }
        $this->saveRequestToken($body['oauth_token'], $body['oauth_token_secret']);
        header('Location: ' . $this->getBaseUrl() . 'security/oauth/confirm_access?oauth_token=' . $body['oauth_token']);
        exit();
    }

    /**
     * request the access token.
     *
     * @param string $verifier
     *
     * @return void
     */
    protected function requestAccessToken($verifier)
    {
        $requestToken = $this->getValidatedRequestToken();
        try {
            $res = $this->callOAuth('security/oauth/access_token', [
                'token' => $requestToken[0],
                'token_secret' => $requestToken[1],
                'verifier' => $verifier,
            ]);
        } catch (ClientException $ex) {
            throw new AuthException('Failed to get the access token');
        }
        parse_str((string) $res->getBody(), $body);
        if (!isset($body['oauth_token']) || !isset($body['oauth_token_secret'])) {
            throw InvalidResponse('Could not parse the requested access token');
        }

        $this->saveAccessToken($body['oauth_token'], $body['oauth_token_secret']);
    }

    /**
     * this method will verify the application, this is needed if the application doesn't have an
     * access token yet, this step only needs to be performed once, general the process looks like this:
     * - getRequest token -> save Request token
     * -> verify with is24 (receive a verifier) (external site that will redirect to the specified callBackUrl)
     * -> get access token (with verifier)
     * -> save access token.
     *
     * @param bool $callBackUrl the url where you want to be redirected after your verification on is24, this
     *                          url needs to recall this method, to consume the oauth_verifier GET variable
     *
     * @return bool if auth is complete it returns true, on the verifiaction step returns false
     */
    public function verifyApplication($callBackUrl = false)
    {
        $key = $this->consumerKey;
        $secret = $this->consumerKeySecret;
        $callBackUrl = $callBackUrl ? $callBackUrl : static::getSelfURI();

        $api = self::createClient($key, $secret, false);

        if (isset($_GET['oauth_verifier']) && isset($_GET['oauth_token'])) {
            $api->requestAccessToken($_GET['oauth_verifier']);

            return true;
        } else {
            $api->requestRequestToken($callBackUrl);

            return false;
        }
    }

    /**
     * returns if the app is verified. this is the case if it can restore the access token.
     *
     * @return boolean
     */
    public function isVerified()
    {
        $validToken = true;
        try {
            $this->getValidatedAccessToken();
        } catch (InvalidTokenException $ex) {
            $validToken = false;
        }

        return $validToken;
    }

    /**
     * get the deep base url, that includes the parsed user.
     *
     * @param string $methodUri
     *
     * @return string url
     */
    protected function getUrl($methodUri, $withUser = true)
    {
        $url = $this->getBaseUrl();
        $path = 'api/offer/v1.0/';

        if ($withUser) {
            $path .= 'user/{user}/';
        }

        if (substr($methodUri, 0, 1) !== '/') {
            $path .= $methodUri;
        } else {
            $path = $methodUri;
        }
        $path = str_replace('{user}', $this->user, $path);

        return $url . $path;
    }

    /**
     * parses the default message body of an response (includes errors and success messages).
     *
     * @param ResponseInterface $res
     *
     * @return array message array
     */
    protected function parseMessages($res)
    {
        $return = [
            'parsed' => [],
        ];
        if (!empty($res->getBody())) {
            try {
                if ($res->getStatusCode() == "401") {
                    throw new AuthException("Immoscout24-API: " . $res->getReasonPhrase());
                }
                $json = json_decode($res->getBody(), true);
            } catch (InvalidArgumentException $ex) {
                $json = [];
            }

            if (isset($json['common.messages']) && is_array($json['common.messages'])) {
                foreach ($json['common.messages'] as $message) {
                    if (isset($message['message']) && isset($message['message'][0])) {
                        foreach ($message['message'] as $msg) {
                            if (isset($msg['messageCode'])) {
                                if (!isset($return[$msg['messageCode']])) {
                                    $return[$msg['messageCode']] = [];
                                }
                                $return[$msg['messageCode']][] = $msg['message'];
                            }
                        }
                    } elseif (isset($message['message']) && isset($message['message']['messageCode'])) {
                        if (!isset($return[$message['message']['messageCode']])) {
                            $return[$message['message']['messageCode']] = [];
                        }
                        $return[$message['message']['messageCode']][] = $message['message']['message'];
                    }
                }
            }

            $parsedErrors = [];

            foreach ($return as $errorCode => $msgArr) {
                foreach ($msgArr as $msg) {
                    $add = null;
                    switch ($errorCode) {
                        case 'ERROR_RESOURCE_VALIDATION':
                            preg_match('/MESSAGE: (.*?) :/', $msg, $matches);
                            if (isset($matches[1])) {
                                $add = $matches[1];
                            } else {
                                preg_match('/MESSAGE: (.*?)\]/', $msg, $matches);
                                if (isset($matches[1])) {
                                    $add = $matches[1];
                                    if ($matches[1] == 'RealEstate already exists for external id.') {
                                        $parsedErrors['ERROR_RESOURCE_DUPLICATE'] = true;
                                    }
                                }
                            }
                            break;
                        case 'MESSAGE_RESOURCE_CREATED':
                            preg_match('/with id \[(.*?)\] /', $msg, $matches);
                            if (isset($matches[1])) {
                                $add = $matches[1];
                            }
                            break;
                        case 'MESSAGE_RESOURCE_UPDATED':
                            $add = '1';
                            break;
                        case 'MESSAGE_RESOURCE_DELETED':
                            $add = '1';
                            break;
                        case 'ERROR_RESOURCE_NOT_FOUND':
                            $add = '404';
                            break;
                    }
                    if ($add) {
                        if (!isset($parsedErrors[$errorCode])) {
                            $parsedErrors[$errorCode] = [];
                        }
                        $parsedErrors[$errorCode][] = $add;
                    }
                }
            }
            $return['parsed'] = $parsedErrors;
        }

        return $return;
    }

    /**
     * make an internal call.
     *
     * @param string $method    like GET, POST, PUT, DELETE
     * @param string $methodUri
     * @param array  $data
     *
     * @return void
     */
    protected function call($method, $methodUri, $data = [], $withUser = false)
    {
        $methodUri = $this->getUrl($methodUri, $withUser);
        try {
            return $this->client->request($method, $methodUri, $data);
        } catch (ClientException $ex) {
            $res = $ex->getResponse();
            $messages = $this->parseMessages($res);

            return $res;
        }
    }

    protected function callMethod($methodUri, $method = 'GET', $jsonData = null, $useJson = true, $withUser = false)
    {
        $data = [];
        if ($jsonData && $useJson) {
            $data = [
                'json' => $jsonData,
            ];
        } elseif ($jsonData) {
            $data = $jsonData;
        }

        return $this->call($method, $methodUri, $data, $withUser);
    }

    protected function callUserMethod($methodUri, $method = 'GET', $jsonData = null, $useJson = true)
    {
        return $this->callMethod($methodUri, $method, $jsonData, $useJson, true);
    }

    /**
     * check for a specific response, if it isn't present a InvalidResponse is thrown.
     *
     * @param ResponseInterface $res
     * @param string            $expectedResponse the expected response
     *
     * @return bool
     */
    protected function checkForResponse($res, $expectedResponse = 'MESSAGE_RESOURCE_CREATED')
    {
        if (is_null($res)) {
            throw new InvalidResponse('Got empty response ');
        }
        $msgs = $this->parseMessages($res);
        if (!isset($msgs['parsed'][$expectedResponse])) {
            $code = isset($msgs['parsed']['ERROR_RESOURCE_NOT_FOUND']) ? 404 : null;
            throw new InvalidResponse('Did not get expected response: ' . $res->getBody(), $code, null, $res, $msgs);
        }
        if ($expectedResponse == 'MESSAGE_RESOURCE_CREATED') {
            return $msgs['parsed'][$expectedResponse][0];
        }

        return true;
    }

    protected function checkForResponseCreated($res)
    {
        return $this->checkForResponse($res, 'MESSAGE_RESOURCE_CREATED');
    }

    protected function checkForResponseUpdated($res)
    {
        return $this->checkForResponse($res, 'MESSAGE_RESOURCE_UPDATED');
    }

    protected function checkForResponseDeleted($res)
    {
        return $this->checkForResponse($res, 'MESSAGE_RESOURCE_DELETED');
    }

    protected function parseGetResponse($res)
    {
        try {
            return json_decode((string) $res->getBody(), true);
        } catch (\Exception$ex) {
            throw new InvalidResponse('invalid json response');
        }
    }

    /**
     * get a list of possible channels, to publish to.
     *
     * @return ResponseInterface
     */
    public function getPublishChannels()
    {
        return $this->callUserMethod('publishchannel');
    }

    /**
     * create a realestate object.
     *
     * @param array $obj realestate object compatible with: https://api.immobilienscout24.de/our-apis/import-export/ftp-vs-api.html
     *
     * @return string id used by immoscout24, this should be saved in order to update the realestate later
     */
    public function createRealEstate($obj)
    {
        $res = $this->callUserMethod('realestate/?usenewenergysourceenev2014values=true', 'POST', $obj);

        return $this->checkForResponseCreated($res);
    }

    /**
     * update a realestate.
     *
     * @param string $id  id used by immoscout
     * @param array  $obj realestate object compatible with: https://api.immobilienscout24.de/our-apis/import-export/ftp-vs-api.html
     *
     * @return bool
     */
    public function updateRealEstate($id, $obj)
    {
        $res = $this->callUserMethod('realestate/' . $id . '?usenewenergysourceenev2014values=true', 'PUT', $obj);

        return $this->checkForResponseUpdated($res);
    }

    /**
     * delete an attachment.
     *
     * @param string $reId         realestate id used by is24
     * @param string $attachmentId attachment id used by is24
     *
     * @return void
     */
    public function deleteAttachment($reId, $attachmentId)
    {
        $res = $this->callUserMethod('realestate/' . $reId . '/attachment/' . $attachmentId, 'DELETE');

        return $this->checkForResponseDeleted($res);
    }

    /**
     * create an attachment for an realestate.
     *
     *
     * @return void
     */

    /**
     * create an attachment for an realestate.
     *
     * @param string $objId          realestate id used by is24
     * @param array  $attachmentData compatible with https://api.immobilienscout24.de/our-apis/import-export/attachments/post.html
     * @param string $content        file content
     * @param string $mimeType       file mime type
     * @param string $fileName       filename (the right extension is important)
     *
     * @return string returns the id that is used by is24, this should be saved
     */
    public function createAttachment($objId, $attachmentData, $content, $mimeType = 'image/jpeg', $fileName = 'image.jpg')
    {
        $res = $this->callUserMethod('realestate/' . $objId . '/attachment/', 'POST', [
            'multipart' => [
                [
                    'Content-type' => 'application/json',
                    'name' => 'metadata',
                    'filename' => 'metadata.json',
                    'contents' => json_encode($attachmentData),
                ], [
                    'Content-type' => $mimeType,
                    'name' => 'attachment',
                    'filename' => $fileName,
                    'contents' => $content,
                ],
            ],
        ], false);

        return $this->checkForResponseCreated($res);
    }

    /**
     * create simple attachment for an realestate, without a file attached.
     *
     * @param string $objId          realestate id used by is24
     * @param array  $attachmentData compatible with https://api.immobilienscout24.de/our-apis/import-export/attachments/post.html
     *
     * @return string returns the id that is used by is24, this should be saved
     */
    public function createSimpleAttachment($objId, $attachmentData)
    {
        $res = $this->callUserMethod('realestate/' . $objId . '/attachment/', 'POST', $attachmentData);

        return $this->checkForResponseCreated($res);
    }

    /**
     * update realestate attachment.
     *
     * @param string $objId          realestate id used by is24
     * @param string $id             id of the attachment used by is24
     * @param array  $attachmentData compatible with https://api.immobilienscout24.de/our-apis/import-export/attachments/post.html
     *
     * @return void
     */
    public function updateAttachment($objId, $id, $attachmentData)
    {
        $res = $this->callUserMethod('realestate/' . $objId . '/attachment/' . $id, 'PUT', $attachmentData);

        return $this->checkForResponseUpdated($res);
    }

    /**
     * Undocumented function.
     *
     * @param string $objId      realestate id used by is24
     * @param array  $orderArray ordered array of the ids used by is24
     *
     * @return void
     */
    public function updateAttachmentOrder($objId, $orderArray)
    {
        $res = $this->callUserMethod('realestate/' . $objId . '/attachment/attachmentsorder', 'PUT', [
            'attachmentsorder.attachmentsorder' => [
                '@xmlns' => [
                    'attachmentsorder' => 'http://rest.immobilienscout24.de/schema/attachmentsorder/1.0',
                ],
                'attachmentId' => $orderArray,
            ],
        ]);

        return $this->checkForResponseUpdated($res);
    }

    /**
     * Get an array of all contacts that can be specified for a real estate.
     *
     * @return array array in the form of https://api.immobilienscout24.de/our-apis/import-export/contact/get-by-id.html
     */
    public function getContacts()
    {
        $res = $this->callUserMethod('contact');
        $arr = $this->parseGetResponse($res);

        if (!isset($arr['common.realtorContactDetailsList']) || !isset($arr['common.realtorContactDetailsList']['realtorContactDetails'])) {
            throw new InvalidResponse('Response doesnt have the expected format');
        }

        return $arr['common.realtorContactDetailsList']['realtorContactDetails'];
    }

    /**
     * Get an array of all contacts that can be specified for a real estate.
     *
     * @param string $reId realestate id used by is24     *
     */
    public function deleteRealestate($reId)
    {
        $res = $this->callUserMethod('realestate/' . $reId, 'DELETE');

        return $this->checkForResponseDeleted($res);
    }

    /**
     * Get an array of all contacts that can be specified for a real estate.
     *
     * @param string $reId           realestate id used by is24
     * @param string $publishchannel publish channel id like 10000
     */
    public function publish($reId, $publishchannel = '10000')
    {
        $res = $this->callMethod('publish/', 'POST', [
            'common.publishObject' => [
                'realEstate' => [
                    '@id' => $reId,
                ],
                'publishChannel' => [
                    '@id' => $publishchannel,
                ],
            ],
        ]);

        return $this->checkForResponseCreated($res);
    }

    /**
     * unpublish a real estate.
     *
     * @param string $reId           realestate id used by is24
     * @param string $publishchannel publish channel id like 10000
     */
    public function unpublish($reId, $publishchannel = '10000')
    {
        $res = $this->callMethod('publish/' . $reId . '_' . $publishchannel, 'DELETE');

        return $this->checkForResponseDeleted($res);
    }
}
