<?php

namespace ut_devops\ImmoScoutAPI\exceptions;

use Exception;

class InvalidTokenException extends Exception
{
}
