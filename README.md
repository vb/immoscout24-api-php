# immoscout24-api-php

An example implementation for the immoscout24 rest api in php. This Client is not complete and only implements some methods for the export/import api, but it should be quite easy to extend.

## install

`composer install ut-devops/immoscout24-api-php`


## usage

The libary contains an abstract class that needs to be extended by an user defined class and implement 4 methods, that are needed to restore and save tokens.

### example implementation

```
class MyAPI extends \ut_devops\ScoutAPI\ImmoScoutAPI
{
    /**
     * should save the request token with tokenname + secret, the token can be temporarily saved within a session
     * this token is only needed during the "request an access token" phase.
     *
     * @param string $token
     * @param string $secret
     *
     * @return void
     */
    public function saveRequestToken($token, $secret){
        $_SESSION['is24reqtoken'] = $token;
        $_SESSION['is24reqsecret'] = $secret;
    }

    /**
     * restores the temporarily saved request token.
     *
     * @return array with 2 elements: first element is the token name, second element is the secret
     */
    public function restoreRequestToken(){
        if(isset($_SESSION['is24reqtoken']) && isset($_SESSION['is24reqsecret'])){
            return [
                $_SESSION['is24reqtoken'],
                $_SESSION['is24reqsecret']
            ];
        }
        return null;
    }

    /**
     * saves the access token, the information should be stored persistently, for example in a database or file.
     * the progress of getting an access token only needs to be done once per user.
     *
     * @param string $token
     * @param string $secret
     *
     * @return void
     */
    public function saveAccessToken($token, $secret){
        file_put_contents('accessToken', serialize([$token, $secret]));
    }

    /**
     * restores the saved access token.
     *
     * @return array with 2 elements: first element is the token name, second element is the secret
     */
    public static function restoreAccessToken(){
        if(file_exists('accessToken')) {
            return unserialize(file_get_contents('accessToken'));
        }
        return null;
    }
}
```

If you want to support multiple user, you can use `$this->getUser()` within the class.

### example usage


```
$key = 'my consumer key';
$secret = 'my consumer secret';

// $api->dontUseSandbox();

$api = MyAPI::createClient($key, $secret);
if ($api->isVerified()) {
    // get a list of possible contacts used in a real estate
    $contactArray = $api->getContacts();
    var_dump($contactArray);
} else {
    // this will trigger the verifiaction with immoscout24, and will try to reconnect
    // to the current url else you can also use a paramater to specify the callback url
    $api->verifyApplication();
}
```